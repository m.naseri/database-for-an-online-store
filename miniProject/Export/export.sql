-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: dbHW
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Customer`
--

DROP TABLE IF EXISTS `Customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Customer` (
  `ID` int(11) NOT NULL,
  `Email` text,
  `Password` text,
  `FirstName` text,
  `LastName` text,
  `Sex` text,
  `Credit` int(11) DEFAULT NULL,
  `Cellphone` text,
  `Address` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Customer`
--

LOCK TABLES `Customer` WRITE;
/*!40000 ALTER TABLE `Customer` DISABLE KEYS */;
INSERT INTO `Customer` VALUES (1,'Customer1@gmail.com','Customer1Pass','Customer1FirstName','Customer1LastName','Male',1000,'+989120000000','AddressOfCustomer1'),(2,'Customer2@gmail.com','Customer2Pass','Customer2FirstName','Customer2LastName','Male',2000,'+989120000000','AddressOfCustomer2'),(3,'Customer3@gmail.com','Customer3Pass','Customer3FirstName','Customer3LastName','Male',0,'+989120000000','AddressOfCustomer3'),(4,'Customer4@gmail.com','Customer4Pass','Customer4FirstName','Customer4LastName','Male',1000,'+989120000000','AddressOfCustomer4'),(5,'Customer5@gmail.com','Customer5Pass','Customer5FirstName','Customer5LastName','Male',1000,'+989120000000','AddressOfCustomer5'),(6,'Customer6@gmail.com','Customer6Pass','Customer6FirstName','Customer6LastName','Male',1000,'+989120000000','AddressOfCustomer6'),(7,'Customer7@gmail.com','Customer7Pass','Customer7FirstName','Customer7LastName','Male',1000,'+989120000000','AddressOfCustomer7'),(8,'Customer8@gmail.com','Customer8Pass','Customer8FirstName','Customer8LastName','Male',1000,'+989120000000','AddressOfCustomer8'),(9,'Customer9@gmail.com','Customer9Pass','Customer9FirstName','Customer9LastName','Male',1000,'+989120000000','AddressOfCustomer9'),(10,'Customer10@gmail.com','Customer10Pass','Customer10FirstName','Customer10LastName','Male',1000,'+989120000000','AddressOfCustomer10'),(11,'Customer11@gmail.com','Customer11Pass','Customer11FirstName','Customer11LastName','Male',1000,'+989120000000','AddressOfCustomer11'),(12,'Customer12@gmail.com','Customer12Pass','Customer12FirstName','Customer12LastName','Male',1000,'+989120000000','AddressOfCustomer12');
/*!40000 ALTER TABLE `Customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Manager`
--

DROP TABLE IF EXISTS `Manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Manager` (
  `ID` int(11) NOT NULL,
  `Email` text,
  `Password` text,
  `FirstName` text,
  `LastName` text,
  `Sex` text,
  `Cellphone` text,
  `Address` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Manager`
--

LOCK TABLES `Manager` WRITE;
/*!40000 ALTER TABLE `Manager` DISABLE KEYS */;
INSERT INTO `Manager` VALUES (1,'Manager1@gmail.com','Manager1Pass','Manager1FirstName','Manager1LastName','Male','+989120000000','AddressOfManager1'),(2,'Manager2@gmail.com','Manager2Pass','Manager2FirstName','Manager2LastName','Male','+989120000000','AddressOfManager2');
/*!40000 ALTER TABLE `Manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order`
--

DROP TABLE IF EXISTS `Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Order` (
  `ID` int(11) NOT NULL,
  `VendorID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `Status` text,
  `PaymentType` text,
  `CreatedAt` date DEFAULT NULL,
  `DeliveryAddress` text,
  PRIMARY KEY (`ID`),
  KEY `VendorID` (`VendorID`),
  KEY `CustomerID` (`CustomerID`),
  CONSTRAINT `Order_ibfk_1` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`),
  CONSTRAINT `Order_ibfk_2` FOREIGN KEY (`CustomerID`) REFERENCES `Customer` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order`
--

LOCK TABLES `Order` WRITE;
/*!40000 ALTER TABLE `Order` DISABLE KEYS */;
INSERT INTO `Order` VALUES (1,1,1,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(2,1,2,'Submitted','Bank','2018-01-01','DeliveryAddress2'),(3,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(4,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(5,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(6,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(7,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(8,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(9,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(10,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(11,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(12,1,1,'Submitted','Bank','2018-01-01','DeliveryAddress1'),(13,1,3,'Submitted','Bank','2018-01-01','DeliveryAddress3'),(14,1,1,'Submitted','Bank','2018-01-01','AddressOfCustomer1'),(15,1,1,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(16,1,2,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(17,1,3,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(18,1,4,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(19,1,5,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(20,1,6,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(21,1,7,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(22,1,8,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(23,1,9,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(24,1,10,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(25,1,11,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(26,2,1,'Submitted','Credit','2018-01-01','DeliveryAddress1'),(27,3,11,'Submitted','Credit','2018-01-01','DeliveryAddress11'),(28,3,10,'Submitted','Credit','2018-01-01','DeliveryAddress10'),(29,1,12,'Submitted','Bank','2017-12-01','DeliveryAddress2'),(30,4,12,'Submitted','Credit','2017-12-01','DeliveryAddress11'),(31,4,12,'Submitted','Credit','2017-12-01','DeliveryAddress10'),(32,4,12,'Submitted','Credit','2017-12-01','DeliveryAddress11'),(33,4,12,'Submitted','Credit','2017-12-01','DeliveryAddress10'),(34,4,12,'Submitted','Credit','2017-12-01','DeliveryAddress10');
/*!40000 ALTER TABLE `Order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderProduct`
--

DROP TABLE IF EXISTS `OrderProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderProduct` (
  `OrderID` int(11) DEFAULT NULL,
  `ProductID` int(11) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  KEY `OrderID` (`OrderID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `OrderProduct_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `Order` (`ID`),
  CONSTRAINT `OrderProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderProduct`
--

LOCK TABLES `OrderProduct` WRITE;
/*!40000 ALTER TABLE `OrderProduct` DISABLE KEYS */;
INSERT INTO `OrderProduct` VALUES (2,1,1),(3,2,1),(4,3,1),(5,4,1),(6,5,1),(7,6,1),(8,7,1),(9,8,1),(10,9,1),(11,10,1),(13,1,2),(15,11,1),(16,11,1),(17,11,1),(18,11,1),(19,11,1),(20,11,1),(21,11,1),(22,11,1),(23,11,1),(24,11,1),(25,11,1),(26,1,3),(27,11,102),(28,11,99),(30,1,1),(30,2,1),(31,1,1),(31,2,1),(32,1,1),(32,3,1),(33,1,1),(33,3,1),(34,1,1),(34,3,1);
/*!40000 ALTER TABLE `OrderProduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ID` int(11) NOT NULL,
  `Title` text,
  `Price` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (1,'TitleOfProduct1',1000),(2,'TitleOfProduct2',1000),(3,'TitleOfProduct3',1000),(4,'TitleOfProduct4',1000),(5,'TitleOfProduct5',1000),(6,'TitleOfProduct6',1000),(7,'TitleOfProduct7',1000),(8,'TitleOfProduct8',1000),(9,'TitleOfProduct9',1000),(10,'TitleOfProduct10',1000),(11,'TitleOfProduct11',1000);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vendor`
--

DROP TABLE IF EXISTS `Vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vendor` (
  `ID` int(11) NOT NULL,
  `Title` text,
  `City` text,
  `ManagerID` int(11) DEFAULT NULL,
  `Address` text,
  `Phone` text,
  PRIMARY KEY (`ID`),
  KEY `ManagerID` (`ManagerID`),
  CONSTRAINT `Vendor_ibfk_1` FOREIGN KEY (`ManagerID`) REFERENCES `Manager` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vendor`
--

LOCK TABLES `Vendor` WRITE;
/*!40000 ALTER TABLE `Vendor` DISABLE KEYS */;
INSERT INTO `Vendor` VALUES (1,'Saadat Abad','Tehran',1,'AddressOfVendor1','+982100000000'),(2,'Vendor2','Tehran',2,'AddressOfVendor2','+982100000000'),(3,'Vendor3','Mashhad',2,'AddressOfVendor3','+982100000000'),(4,'Pasdaran','Tehran',1,'AddressOfVendor4','+982100000000');
/*!40000 ALTER TABLE `Vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VendorProduct`
--

DROP TABLE IF EXISTS `VendorProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VendorProduct` (
  `VendorID` int(11) DEFAULT NULL,
  `ProductID` int(11) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  KEY `VendorID` (`VendorID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `VendorProduct_ibfk_1` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`),
  CONSTRAINT `VendorProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VendorProduct`
--

LOCK TABLES `VendorProduct` WRITE;
/*!40000 ALTER TABLE `VendorProduct` DISABLE KEYS */;
INSERT INTO `VendorProduct` VALUES (1,1,1),(1,2,1),(1,3,1),(1,4,1),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,10,1),(1,11,1);
/*!40000 ALTER TABLE `VendorProduct` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-25 20:16:25
