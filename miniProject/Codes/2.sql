-- Q1
insert into Manager values(1, "Manager1@gmail.com", 
	"Manager1Pass", "Manager1FirstName", 
	"Manager1LastName", "Male", "+989120000000", "AddressOfManager1");
insert into Vendor values(1, "Saadat Abad", "Tehran", 1, "AddressOfVendor1", "+982100000000");

insert into Manager values(2, "Manager2@gmail.com", 
	"Manager2Pass", "Manager2FirstName", 
	"Manager2LastName", "Male", "+989120000000", "AddressOfManager2");
insert into Vendor values(2, "Vendor2", "Tehran", 2, "AddressOfVendor2", "+982100000000");
insert into Vendor values(3, "Vendor3", "Mashhad", 2, "AddressOfVendor3", "+982100000000");

-- Q2
insert into Customer values(1, "Customer1@gmail.com", "Customer1Pass",
	"Customer1FirstName", "Customer1LastName", 
	"Male", "1000", "+989120000000", "AddressOfCustomer1");
insert into Customer values(2, "Customer2@gmail.com", "Customer2Pass",
	"Customer2FirstName", "Customer2LastName", 
	"Male", "2000", "+989120000000", "AddressOfCustomer2");

insert into `Order` values(1, 1, 1, "Submitted", "Credit",
 	"2018-01-01", "DeliveryAddress1");
insert into `Order` values(2, 1, 2, "Submitted", "Bank",
 	"2018-01-01", "DeliveryAddress2");

-- Q3
insert into Product 
	values
	(1, "TitleOfProduct1", 1000),
	(2, "TitleOfProduct2", 1000),
	(3, "TitleOfProduct3", 1000),
	(4, "TitleOfProduct4", 1000),
	(5, "TitleOfProduct5", 1000),
	(6, "TitleOfProduct6", 1000),
	(7, "TitleOfProduct7", 1000),
	(8, "TitleOfProduct8", 1000),
	(9, "TitleOfProduct9", 1000),
	(10, "TitleOfProduct10", 1000),
	(11, "TitleOfProduct11", 1000);

insert into VendorProduct
	values
	(1, 1, 1),
	(1, 2, 1),
	(1, 3, 1),
	(1, 4, 1),
	(1, 5, 1),
	(1, 6, 1),
	(1, 7, 1),
	(1, 8, 1),
	(1, 9, 1),
	(1, 10, 1),
	(1, 11, 1);

-- Q4
insert into `Order`
	values
	(3, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(4, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(5, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(6, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(7, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(8, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(9, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(10, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(11, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1'),
	(12, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress1');

insert into OrderProduct
	values
	(2, 1, 1),
	(3, 2, 1),
	(4, 3, 1),
	(5, 4, 1),
	(6, 5, 1),
	(7, 6, 1),
	(8, 7, 1),
	(9, 8, 1),
	(10, 9, 1),
	(11, 10, 1),
	(12, 11, 1);

-- Q5
insert into Customer values(3, "Customer3@gmail.com", "Customer3Pass",
	"Customer3FirstName", "Customer3LastName", 
	"Male", "0", "+989120000000", "AddressOfCustomer3");

insert into `Order`
	values
	(13, 1, 3, 'Submitted', 'Bank', '2018-01-01', 'DeliveryAddress3');

insert into OrderProduct
	values
	(13, 1, 2);

-- Q6
insert into `Order`
	values
	(14, 1, 1, 'Submitted', 'Bank', '2018-01-01', 'AddressOfCustomer1');

-- Q7
insert into Product 
	values
	(12, "TitleOfProduct12", 1000);

insert into Customer values
	(4, "Customer4@gmail.com", "Customer4Pass", "Customer4FirstName", 
		"Customer4LastName", "Male", "1000", "+989120000000", "AddressOfCustomer4"),
	(5, "Customer5@gmail.com", "Customer5Pass", "Customer5FirstName", 
		"Customer5LastName", "Male", "1000", "+989120000000", "AddressOfCustomer5"),
	(6, "Customer6@gmail.com", "Customer6Pass", "Customer6FirstName", 
		"Customer6LastName", "Male", "1000", "+989120000000", "AddressOfCustomer6"),
	(7, "Customer7@gmail.com", "Customer7Pass", "Customer7FirstName", 
		"Customer7LastName", "Male", "1000", "+989120000000", "AddressOfCustomer7"),
	(8, "Customer8@gmail.com", "Customer8Pass", "Customer8FirstName", 
		"Customer8LastName", "Male", "1000", "+989120000000", "AddressOfCustomer8"),
	(9, "Customer9@gmail.com", "Customer9Pass", "Customer9FirstName", 
		"Customer9LastName", "Male", "1000", "+989120000000", "AddressOfCustomer9"),
	(10, "Customer10@gmail.com", "Customer10Pass", "Customer10FirstName", 
		"Customer10LastName", "Male", "1000", "+989120000000", "AddressOfCustomer10"),
	(11, "Customer11@gmail.com", "Customer11Pass", "Customer11FirstName", 
		"Customer11LastName", "Male", "1000", "+989120000000", "AddressOfCustomer11");

insert into `Order` values
	(15, 1, 1, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(16, 1, 2, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(17, 1, 3, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(18, 1, 4, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(19, 1, 5, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(20, 1, 6, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(21, 1, 7, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(22, 1, 8, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(23, 1, 9, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(24, 1, 10, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1"),
	(25, 1, 11, "Submitted", "Credit", "2018-01-01", "DeliveryAddress1");


insert into OrderProduct values
	(15, 11, 1),
	(16, 11, 1),
	(17, 11, 1),
	(18, 11, 1),
	(19, 11, 1),
	(20, 11, 1),
	(21, 11, 1),
	(22, 11, 1),
	(23, 11, 1),
	(24, 11, 1),
	(25, 11, 1);

-- Q8
insert into `Order` values(26, 2, 1, "Submitted", "Credit",
 	"2018-01-01", "DeliveryAddress1");

insert into OrderProduct values(26, 1, 3);

-- Q9
insert into `Order` values
	(27, 3, 11, "Submitted", "Credit", "2018-01-01", "DeliveryAddress11"),
	(28, 3, 10, "Submitted", "Credit", "2018-01-01", "DeliveryAddress10");

insert into OrderProduct
values
	(27, 11, 102),
	(28, 11, 99);

-- Q11
insert into Customer values(12, "Customer12@gmail.com", "Customer12Pass",
	"Customer12FirstName", "Customer12LastName", 
	"Male", "1000", "+989120000000", "AddressOfCustomer12");

insert into `Order` values(29, 1, 12, "Submitted", "Bank",
 	"2017-12-01", "DeliveryAddress2");

-- Q12
insert into Vendor values(4, "Pasdaran", "Tehran", 1, "AddressOfVendor4", "+982100000000");

insert into `Order` values
	(30, 4, 12, "Submitted", "Credit", "2017-12-01", "DeliveryAddress11"),
	(31, 4, 12, "Submitted", "Credit", "2017-12-01", "DeliveryAddress10"),
	(32, 4, 12, "Submitted", "Credit", "2017-12-01", "DeliveryAddress11"),
	(33, 4, 12, "Submitted", "Credit", "2017-12-01", "DeliveryAddress10"),
	(34, 4, 12, "Submitted", "Credit", "2017-12-01", "DeliveryAddress10");

insert into OrderProduct
values
	(30, 1, 1),
	(30, 2, 1),
	(31, 1, 1),
	(31, 2, 1),
	(32, 1, 1),
	(32, 3, 1),
	(33, 1, 1),
	(33, 3, 1),
	(34, 1, 1),
	(34, 3, 1);
