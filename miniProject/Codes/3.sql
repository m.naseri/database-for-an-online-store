-- Q1
select *
from Vendor
where City = "Tehran";

-- Q2
select distinct C.*
from Customer C, `Order` O
where O.CustomerID = C.ID and O.PaymentType = "Credit";

-- Q3
select *
from Vendor
where Vendor.ID in (
select VendorID
from VendorProduct
group by VendorID
having count(distinct ProductID) > 10);

-- Q4
select Vendor.*, Product.ID as 'ProductID', 
	Product.Title as 'ProductTitle', Product.Price as 'ProductPrice'
from Vendor, Product, `Order`, OrderProduct
where OrderProduct.OrderID = `Order`.ID and
	  Order.VendorID = Vendor.ID and
	  Product.ID = OrderProduct.ProductID and
	  Vendor.ID in (
	  	select VendorID
	  	from `Order`, OrderProduct
	  	where OrderProduct.OrderID = `Order`.ID
	  	group by VendorID
	  	having count(distinct ProductID) > 10);

-- Q5
select distinct O1.CustomerID as CustomerID1, O2.CustomerID as CustomerID2
from OrderProduct OP1, OrderProduct OP2,
	 `Order` O1, `Order` O2
where O1.CustomerID < O2.CustomerID and OP1.ProductID = OP2.ProductID
	  and O1.ID = OP1.OrderID and O2.ID = OP2.OrderID;

-- Q6
select Customer.*
from Customer, `Order`
where Customer.ID = `Order`.CustomerID 
	and Customer.Address = Order.DeliveryAddress;

-- Q7
select *
from Product
where Product.ID in (
	select OP.ProductID
	from OrderProduct OP, `Order` O
	where OP.OrderID = O.ID
	group by OP.ProductID
	having count(distinct O.ID) > 10);

-- Q8
select Customer.*
from `Order` O, OrderProduct OP, Customer
where O.ID = OP.OrderID and Customer.ID = O.CustomerID
	  and O.CreatedAt >= '2018-01-01'
group by CustomerID
having count(distinct O.VendorID) > 1;

-- Q9
select Vendor.*
from (select O.VendorID, O.CustomerID, sum(OP.Amount * P.Price) as Total
	from OrderProduct OP, Product P, `Order` O
	where OP.OrderID = O.ID and OP.ProductID = P.ID
	group by VendorID, CustomerID) as A, Vendor
where A.VendorID = Vendor.ID
group by A.VendorID
having avg(A.Total) > 100000;

-- Q10
select P.*
from OrderProduct OP, Product P, `Order` O, Vendor V
where OP.OrderID = O.ID and OP.ProductID = P.ID
	  and V.ID = O.VendorID and V.Title = 'Saadat Abad'
group by P.ID
order by sum(OP.Amount) desc
limit 1 offset 2;

-- Q11
select distinct C.*
from `Order` O, Customer C
where O.CreatedAt < '2018-01-01' and O.CustomerID = C.ID and
C.ID not in (select distinct C2.ID
	from `Order` O2, Customer C2
	where O2.CreatedAt >= '2018-01-01' and O2.CustomerID = C2.ID);

-- Q12
select OP1.ProductID as ProductID1, OP2.ProductID as ProductID2
from OrderProduct OP1, OrderProduct OP2, `Order` O, Vendor V
where OP1.OrderID = OP2.OrderID and OP1.ProductID < OP2.ProductID
	  and O.VendorID = V.ID and O.ID = OP1.OrderID
	  and V.Title = 'Pasdaran'
group by OP1.ProductID, OP2.ProductID
order by count(O.ID) desc
limit 1 offset 0;