-- Section 1
create table Customer(
	ID int primary key,
	Email text,
	Password text,
	FirstName text,
	LastName text,
	Sex text,
	Credit int,
	Cellphone text,
	Address text
);

create table Manager(
	ID int primary key, 
	Email text,
	Password text, 
	FirstName text, 
	LastName text,
	Sex text, 
	Cellphone text, 
	Address text
);

create table Vendor(
	ID int primary key,
	Title text,
	City text,
	ManagerID int,
	Address text, 
	Phone text,
	foreign key (ManagerID) references Manager(ID)
);

create table `Order`(
	ID int primary key, 
	VendorID int, 
	CustomerID int,
	Status text,
	PaymentType text,
	CreatedAt date,
	DeliveryAddress text,
	foreign key (VendorID) references Vendor(ID),
	foreign key (CustomerID) references Customer(ID)
);

create table Product(
	ID int primary key, 
	Title text, 
	Price int
);

create table VendorProduct(
	VendorID int, 
	ProductID int, 
	Amount int,
	foreign key (VendorID) references Vendor(ID),
	foreign key (ProductID) references Product(ID)
);

create table OrderProduct(
	OrderID int,
	ProductID int,
	Amount int,
	foreign key (OrderID) references `Order`(ID),
	foreign key (ProductID) references Product(ID)
);