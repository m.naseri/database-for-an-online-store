-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: naseriFinalDB
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Customer`
--

DROP TABLE IF EXISTS `Customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Customer` (
  `Username` varchar(20) NOT NULL,
  `Email` text,
  `Password` text,
  `FirstName` text,
  `LastName` text,
  `Sex` text,
  `Credit` int(11) DEFAULT NULL,
  `PostalCode` text,
  PRIMARY KEY (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Customer`
--

LOCK TABLES `Customer` WRITE;
/*!40000 ALTER TABLE `Customer` DISABLE KEYS */;
INSERT INTO `Customer` VALUES ('C1Username','c1@gmail.com','f12152a12c58bd8af282a8c794f3511da26ba7b4','C1Firstname','C1LastName','M',1000,'1000000000'),('C2Username','c2@gmail.com','4018aca26cead3f5ac1770de29763736885de4ed','C2Firstname','C2LastName','M',1000,'1000000000');
/*!40000 ALTER TABLE `Customer` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger userCanOnlyChangeHisAccount
before update on Customer for each row
begin
if old.Username != SUBSTRING_INDEX(user(), '@', 1) and SUBSTRING_INDEX(user(), '@', 1) != 'root'
then
signal sqlstate '45000'
set message_text = 'You can only change your own account.';
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `CustomerAddress`
--

DROP TABLE IF EXISTS `CustomerAddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CustomerAddress` (
  `Username` varchar(20) NOT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `ithAddress` int(11) NOT NULL,
  PRIMARY KEY (`Username`,`ithAddress`),
  CONSTRAINT `CustomerAddress_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `Customer` (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CustomerAddress`
--

LOCK TABLES `CustomerAddress` WRITE;
/*!40000 ALTER TABLE `CustomerAddress` DISABLE KEYS */;
INSERT INTO `CustomerAddress` VALUES ('C1Username','C1Address1',1);
/*!40000 ALTER TABLE `CustomerAddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CustomerPhone`
--

DROP TABLE IF EXISTS `CustomerPhone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CustomerPhone` (
  `Username` varchar(20) NOT NULL,
  `PhoneNumber` varchar(15) NOT NULL,
  PRIMARY KEY (`Username`,`PhoneNumber`),
  CONSTRAINT `CustomerPhone_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `Customer` (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CustomerPhone`
--

LOCK TABLES `CustomerPhone` WRITE;
/*!40000 ALTER TABLE `CustomerPhone` DISABLE KEYS */;
INSERT INTO `CustomerPhone` VALUES ('C1Username','+982100000091');
/*!40000 ALTER TABLE `CustomerPhone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DeliveryAgent`
--

DROP TABLE IF EXISTS `DeliveryAgent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeliveryAgent` (
  `ID` int(11) NOT NULL,
  `FirstName` text,
  `LastName` text,
  `phonenumber` text,
  `isAvailable` int(11) DEFAULT NULL,
  `Credit` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeliveryAgent`
--

LOCK TABLES `DeliveryAgent` WRITE;
/*!40000 ALTER TABLE `DeliveryAgent` DISABLE KEYS */;
INSERT INTO `DeliveryAgent` VALUES (1,'DA1FirstName','DA1LastName','+989220000001',1,0),(2,'DA2FirstName','DA2LastName','+989220000001',0,50);
/*!40000 ALTER TABLE `DeliveryAgent` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger logDeliveryAgentisAvailable
after update on DeliveryAgent for each row
begin
if new.isAvailable != old.isAvailable
then
insert into DeliveryAgentStatusLog values
(new.ID, old.isAvailable, new.isAvailable, now());
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `DeliveryAgentStatusLog`
--

DROP TABLE IF EXISTS `DeliveryAgentStatusLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DeliveryAgentStatusLog` (
  `DAID` int(11) NOT NULL,
  `oldIsAvailable` int(11) DEFAULT NULL,
  `newISAvailable` int(11) DEFAULT NULL,
  `DateAndTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`DAID`,`DateAndTime`),
  CONSTRAINT `DeliveryAgentStatusLog_ibfk_1` FOREIGN KEY (`DAID`) REFERENCES `DeliveryAgent` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DeliveryAgentStatusLog`
--

LOCK TABLES `DeliveryAgentStatusLog` WRITE;
/*!40000 ALTER TABLE `DeliveryAgentStatusLog` DISABLE KEYS */;
INSERT INTO `DeliveryAgentStatusLog` VALUES (2,1,0,'2019-01-18 13:40:43'),(2,0,1,'2019-01-18 13:43:46'),(2,1,0,'2019-01-18 14:00:36');
/*!40000 ALTER TABLE `DeliveryAgentStatusLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OperationAgent`
--

DROP TABLE IF EXISTS `OperationAgent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OperationAgent` (
  `ID` int(11) NOT NULL,
  `FirstName` text,
  `LastName` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OperationAgent`
--

LOCK TABLES `OperationAgent` WRITE;
/*!40000 ALTER TABLE `OperationAgent` DISABLE KEYS */;
/*!40000 ALTER TABLE `OperationAgent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderProduct`
--

DROP TABLE IF EXISTS `OrderProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderProduct` (
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`OrderID`,`ProductID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `OrderProduct_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `StoreOrder` (`ID`),
  CONSTRAINT `OrderProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderProduct`
--

LOCK TABLES `OrderProduct` WRITE;
/*!40000 ALTER TABLE `OrderProduct` DISABLE KEYS */;
INSERT INTO `OrderProduct` VALUES (1,1,1),(1,2,2),(1,3,3),(1,4,4),(1,5,5),(1,6,6),(2,1,1);
/*!40000 ALTER TABLE `OrderProduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ID` int(11) NOT NULL,
  `title` text,
  `StoreID` int(11) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `Discount` int(11) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `StoreID` (`StoreID`),
  CONSTRAINT `Product_ibfk_1` FOREIGN KEY (`StoreID`) REFERENCES `Store` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (1,'Product1',1,1000,0,8),(2,'Product2',1,2000,0,10),(3,'Product3',1,3000,0,10),(4,'Product4',1,4000,0,10),(5,'Product5',1,5000,0,10),(6,'Product6',1,6000,0,10);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `Q1A`
--

DROP TABLE IF EXISTS `Q1A`;
/*!50001 DROP VIEW IF EXISTS `Q1A`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `Q1A` AS SELECT 
 1 AS `SID`,
 1 AS `PID`,
 1 AS `C`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `Q3A`
--

DROP TABLE IF EXISTS `Q3A`;
/*!50001 DROP VIEW IF EXISTS `Q3A`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `Q3A` AS SELECT 
 1 AS `OID`,
 1 AS `CUN`,
 1 AS `price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `Q3B`
--

DROP TABLE IF EXISTS `Q3B`;
/*!50001 DROP VIEW IF EXISTS `Q3B`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `Q3B` AS SELECT 
 1 AS `OID`,
 1 AS `CUN`,
 1 AS `price`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ReasonForRejection`
--

DROP TABLE IF EXISTS `ReasonForRejection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReasonForRejection` (
  `OrderID` int(11) NOT NULL,
  `reason` text,
  PRIMARY KEY (`OrderID`),
  CONSTRAINT `ReasonForRejection_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `StoreOrder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ReasonForRejection`
--

LOCK TABLES `ReasonForRejection` WRITE;
/*!40000 ALTER TABLE `ReasonForRejection` DISABLE KEYS */;
INSERT INTO `ReasonForRejection` VALUES (2,'Store is closed now');
/*!40000 ALTER TABLE `ReasonForRejection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Store`
--

DROP TABLE IF EXISTS `Store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Store` (
  `ID` int(11) NOT NULL,
  `title` text,
  `ciry` text,
  `phonenumber` text,
  `manager` text,
  `OpenTime` time DEFAULT NULL,
  `CloseTime` time DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Store`
--

LOCK TABLES `Store` WRITE;
/*!40000 ALTER TABLE `Store` DISABLE KEYS */;
INSERT INTO `Store` VALUES (1,'Store1Title','Store1City','+982100000001','Stroe1Manager','08:00:00','14:00:00'),(2,'Store2Title','Store2City','+982100000002','Stroe2Manager','09:00:00','18:00:00');
/*!40000 ALTER TABLE `Store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StoreDeliveryAgent`
--

DROP TABLE IF EXISTS `StoreDeliveryAgent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StoreDeliveryAgent` (
  `StoreID` int(11) NOT NULL,
  `DeliveryAgentID` int(11) NOT NULL,
  PRIMARY KEY (`StoreID`,`DeliveryAgentID`),
  KEY `DeliveryAgentID` (`DeliveryAgentID`),
  CONSTRAINT `StoreDeliveryAgent_ibfk_1` FOREIGN KEY (`StoreID`) REFERENCES `Store` (`ID`),
  CONSTRAINT `StoreDeliveryAgent_ibfk_2` FOREIGN KEY (`DeliveryAgentID`) REFERENCES `DeliveryAgent` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StoreDeliveryAgent`
--

LOCK TABLES `StoreDeliveryAgent` WRITE;
/*!40000 ALTER TABLE `StoreDeliveryAgent` DISABLE KEYS */;
/*!40000 ALTER TABLE `StoreDeliveryAgent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StoreOperationAgent`
--

DROP TABLE IF EXISTS `StoreOperationAgent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StoreOperationAgent` (
  `StoreID` int(11) NOT NULL,
  `OperationAgentID` int(11) NOT NULL,
  PRIMARY KEY (`StoreID`,`OperationAgentID`),
  KEY `OperationAgentID` (`OperationAgentID`),
  CONSTRAINT `StoreOperationAgent_ibfk_1` FOREIGN KEY (`StoreID`) REFERENCES `Store` (`ID`),
  CONSTRAINT `StoreOperationAgent_ibfk_2` FOREIGN KEY (`OperationAgentID`) REFERENCES `OperationAgent` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StoreOperationAgent`
--

LOCK TABLES `StoreOperationAgent` WRITE;
/*!40000 ALTER TABLE `StoreOperationAgent` DISABLE KEYS */;
/*!40000 ALTER TABLE `StoreOperationAgent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StoreOrder`
--

DROP TABLE IF EXISTS `StoreOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StoreOrder` (
  `ID` int(11) NOT NULL,
  `StoreID` int(11) DEFAULT NULL,
  `CustomerUsername` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `PaymentType` int(11) DEFAULT NULL,
  `DateAndTime` datetime DEFAULT NULL,
  `ithAddress` int(11) DEFAULT NULL,
  `DeliveryAgentID` int(11) DEFAULT NULL,
  `NotRegisteredUsersAddress` text,
  PRIMARY KEY (`ID`),
  KEY `CustomerUsername` (`CustomerUsername`,`ithAddress`),
  KEY `DeliveryAgentID` (`DeliveryAgentID`),
  KEY `StoreID` (`StoreID`),
  CONSTRAINT `StoreOrder_ibfk_1` FOREIGN KEY (`CustomerUsername`, `ithAddress`) REFERENCES `CustomerAddress` (`Username`, `ithAddress`),
  CONSTRAINT `StoreOrder_ibfk_2` FOREIGN KEY (`DeliveryAgentID`) REFERENCES `DeliveryAgent` (`ID`),
  CONSTRAINT `StoreOrder_ibfk_3` FOREIGN KEY (`StoreID`) REFERENCES `Store` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StoreOrder`
--

LOCK TABLES `StoreOrder` WRITE;
/*!40000 ALTER TABLE `StoreOrder` DISABLE KEYS */;
INSERT INTO `StoreOrder` VALUES (1,1,NULL,0,1,'2019-01-18 16:02:47',NULL,1,'Address1'),(2,1,'C1Username',2,0,'2019-01-18 16:03:00',1,2,NULL);
/*!40000 ALTER TABLE `StoreOrder` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger notRegisteredUsersCreditPayment
before update on StoreOrder for each row
begin
if new.status = 1 and old.status != 1 and 
   new.CustomerUsername is null and new.PaymentType = 0
then
signal sqlstate '45000'
set message_text = 'Not-registered users cannot pay with their credit.';
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger rejectClosedStore
before update on StoreOrder for each row
begin
if new.status = 1 and old.status != 1 and (
time(new.DateAndTime) < (select OpenTime from Store S where S.ID = new.StoreID)
or 
time(new.DateAndTime) > (select CloseTime from Store S where S.ID = new.StoreID)
)
then
set new.status = -1;
insert into ReasonForRejection values(new.ID, 'Store is closed now');
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger decreaseAmountOfProductsAfterSending
before update on StoreOrder for each row
begin
if new.status = 2 and old.status != 2
then
update Product P
set P.Amount = P.Amount - (
select OP.Amount 
from OrderProduct OP 
where OP.OrderID = new.ID and OP.ProductID = P.ID)
where P.ID in (
select OP.ProductID
from OrderProduct OP
where OP.OrderID = new.ID);
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger updateCreditOfCustomerSubmitOrder
after update on StoreOrder for each row
begin
if new.status = 1 and old.status != 1 and new.PaymentType = 0
and new.CustomerUsername is not null
then
update Customer
set Credit = Credit - (
select B.price
from Q3B B
where B.OID = new.ID)
where Customer.Username = new.CustomerUsername;
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger setDeliveryAgentNotAvailableAfterSending
after update on StoreOrder for each row
begin
if new.status = 2 and old.status != 2
then
update DeliveryAgent DA
set DA.isAvailable = 0
where DA.ID = new.DeliveryAgentID;
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger setDeliveryAgentAvailableAfterCompleted
after update on StoreOrder for each row
begin
if new.status = 3 and old.status != 3
then
update DeliveryAgent DA
set DA.isAvailable = 1,
DA.Credit = DA.Credit + 
(5/100)*(select B.price from Q3B B where B.OID = new.ID)
where DA.ID = new.DeliveryAgentID;
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger logStoreOrderStatus
after update on StoreOrder for each row
begin
if new.status != old.status
then
insert into StoreOrderStatusLog values(new.ID, old.status, new.status, now());
end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `StoreOrderStatusLog`
--

DROP TABLE IF EXISTS `StoreOrderStatusLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StoreOrderStatusLog` (
  `OID` int(11) NOT NULL,
  `oldStatus` int(11) DEFAULT NULL,
  `newStatus` int(11) DEFAULT NULL,
  `DateAndTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`OID`,`DateAndTime`),
  CONSTRAINT `StoreOrderStatusLog_ibfk_1` FOREIGN KEY (`OID`) REFERENCES `StoreOrder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StoreOrderStatusLog`
--

LOCK TABLES `StoreOrderStatusLog` WRITE;
/*!40000 ALTER TABLE `StoreOrderStatusLog` DISABLE KEYS */;
INSERT INTO `StoreOrderStatusLog` VALUES (2,-1,1,'2019-01-18 13:29:15'),(2,1,0,'2019-01-18 13:29:52'),(2,0,1,'2019-01-18 13:30:26'),(2,1,0,'2019-01-18 13:31:43'),(2,0,-1,'2019-01-18 13:34:31'),(2,-1,2,'2019-01-18 13:40:43'),(2,2,3,'2019-01-18 13:43:46'),(2,3,0,'2019-01-18 13:45:04'),(2,0,2,'2019-01-18 14:00:36'),(2,2,0,'2019-01-18 14:00:59'),(2,0,2,'2019-01-18 14:01:26');
/*!40000 ALTER TABLE `StoreOrderStatusLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StoreSupportAgent`
--

DROP TABLE IF EXISTS `StoreSupportAgent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StoreSupportAgent` (
  `StoreID` int(11) NOT NULL,
  `SupportAgentID` int(11) NOT NULL,
  PRIMARY KEY (`StoreID`,`SupportAgentID`),
  KEY `SupportAgentID` (`SupportAgentID`),
  CONSTRAINT `StoreSupportAgent_ibfk_1` FOREIGN KEY (`StoreID`) REFERENCES `Store` (`ID`),
  CONSTRAINT `StoreSupportAgent_ibfk_2` FOREIGN KEY (`SupportAgentID`) REFERENCES `SupportAgent` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StoreSupportAgent`
--

LOCK TABLES `StoreSupportAgent` WRITE;
/*!40000 ALTER TABLE `StoreSupportAgent` DISABLE KEYS */;
/*!40000 ALTER TABLE `StoreSupportAgent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SupportAgent`
--

DROP TABLE IF EXISTS `SupportAgent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SupportAgent` (
  `ID` int(11) NOT NULL,
  `FirstName` text,
  `LastName` text,
  `phonenumber` text,
  `Address` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SupportAgent`
--

LOCK TABLES `SupportAgent` WRITE;
/*!40000 ALTER TABLE `SupportAgent` DISABLE KEYS */;
/*!40000 ALTER TABLE `SupportAgent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `Q1A`
--

/*!50001 DROP VIEW IF EXISTS `Q1A`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `Q1A` AS select `P2`.`StoreID` AS `SID`,`temp`.`PID` AS `PID`,`temp`.`C` AS `C` from (`naseriFinalDB`.`Product` `P2` join (select `naseriFinalDB`.`Product`.`ID` AS `PID`,sum(`naseriFinalDB`.`OrderProduct`.`Amount`) AS `C` from (`naseriFinalDB`.`Product` join `naseriFinalDB`.`OrderProduct`) where (`naseriFinalDB`.`Product`.`ID` = `naseriFinalDB`.`OrderProduct`.`ProductID`) group by `naseriFinalDB`.`Product`.`ID`) `temp`) where (`P2`.`ID` = `temp`.`PID`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `Q3A`
--

/*!50001 DROP VIEW IF EXISTS `Q3A`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `Q3A` AS select `O`.`ID` AS `OID`,`O`.`CustomerUsername` AS `CUN`,((`OP`.`Amount` * (1 - (`P`.`Discount` / 100))) * `P`.`Price`) AS `price` from ((`StoreOrder` `O` join `OrderProduct` `OP`) join `Product` `P`) where ((`O`.`ID` = `OP`.`OrderID`) and (`OP`.`ProductID` = `P`.`ID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `Q3B`
--

/*!50001 DROP VIEW IF EXISTS `Q3B`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `Q3B` AS select `A`.`OID` AS `OID`,`A`.`CUN` AS `CUN`,sum(`A`.`price`) AS `price` from `Q3A` `A` group by `A`.`OID`,`A`.`CUN` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18 23:50:11
