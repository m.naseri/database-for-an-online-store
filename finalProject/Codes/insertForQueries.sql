--Q1
insert into Store values(1, "Store1Title", "Store1City", 
	"+982100000001", "Stroe1Manager", "08:00:00", "14:00:00");

insert into StoreOrder values(1, 1, null, 0, 1, now(), null, null, "Address1");

insert into Product values
	(1, "Product1", 1, 1000, 0, 10),
	(2, "Product2", 1, 2000, 0, 10),
	(3, "Product3", 1, 3000, 0, 10),
	(4, "Product4", 1, 4000, 0, 10),
	(5, "Product5", 1, 5000, 0, 10),
	(6, "Product6", 1, 6000, 0, 10);

insert into OrderProduct values
	(1, 1, 1),
	(1, 2, 2),
	(1, 3, 3),
	(1, 4, 4),
	(1, 5, 5),
	(1, 6, 6);

--Q2
insert into Customer values('C1Username', 'c1@gmail.com', SHA1('C1Pass'), 
	'C1Firstname', 'C1LastName', 'M', 1000, '1000000000');

insert into CustomerAddress values('C1Username', 'C1Address1', 1);

insert into CustomerPhone values('C1Username', '+982100000091');

insert into StoreOrder values(2, 1, 'C1Username', -1, 1, now(), 1, null, null);

--Q3
insert into OrderProduct values(2, 1, 1);

--Q4
insert into DeliveryAgent values
	(1, 'DA1FirstName', 'DA1LastName', '+989220000001', 1, 0),
	(2, 'DA2FirstName', 'DA2LastName', '+989220000001', 1, 0);
update StoreOrder set DeliveryAgentID = 1 where ID = 1;
update StoreOrder set DeliveryAgentID = 2 where ID = 2;

--Q5
insert into Store values(2, "Store2Title", "Store2City", 
	"+982100000002", "Stroe2Manager", "09:00:00", "18:00:00");