--Q1
create view Q1A as
select P2.StoreID as SID, temp.PID as PID, temp.C as C
from Product P2, (select Product.ID as PID, sum(OrderProduct.Amount) as C
	from Product, OrderProduct
	where Product.ID = OrderProduct.ProductID
	group by Product.ID
) as temp
where P2.ID = PID;

select A.SID, A.PID, A.C
from Q1A as A, Q1A as B
where A.SID = B.SID and A.C < B.C
group by A.SID, A.PID, A.C
having count(B.C) < 5
order by A.SID;

--Q2
select CP.*
from StoreOrder O, Customer C, CustomerPhone CP
where O.CustomerUsername = C.Username and O.status = -1 and CP.Username = C.Username;

--Q3
create view Q3A as
select O.ID as OID, O.CustomerUsername as CUN, OP.Amount
	*(1 - P.Discount/100)*P.Price as price
from StoreOrder O, OrderProduct OP, Product P
where O.ID = OP.OrderID and OP.ProductID = P.ID;

	--Total Price of each Order 
create view Q3B as
select A.OID as OID, A.CUN as CUN, sum(A.price) as price
from Q3A as A
group by A.OID, A.CUN;

select
(select avg(B.price)
from Q3B as B
where B.CUN is null
group by B.OID, B.CUN) - 
(select avg(B.price)
from Q3B as B
where B.CUN is not null
group by B.OID, B.CUN);

--Q4
select DA.*, A.price
from (
	select DA.ID as ID, avg(price) as price
	from Q3B as B, StoreOrder O, DeliveryAgent DA
	where B.OID = O.ID and DA.ID = O.DeliveryAgentID
	group by DA.ID
) as A, DeliveryAgent DA
where DA.ID = A.ID
order by A.price desc
limit 1;

--Q5
select Store.*
from Store
order by (CloseTime - OpenTime) desc limit 1;