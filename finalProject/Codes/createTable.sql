create database naseriFinalDB;

create table Customer(
	Username VARCHAR(20) primary key,
	Email text,
	Password text,
	FirstName text,
	LastName text,
	Sex text,
	Credit int,
	PostalCode text
);

create table CustomerAddress(
	Username VARCHAR(20),
	Address VARCHAR(200),
	ithAddress int,
	primary key(Username, ithAddress),
	foreign key (Username) references Customer(Username)
);

create table CustomerPhone(
	Username VARCHAR(20),
	PhoneNumber VARCHAR(15),
	primary key(Username, PhoneNumber),
	foreign key (Username) references Customer(Username)
);

create table Store(
	ID int primary key,
	title text,
	ciry text,
	phonenumber text,
	manager text,
	OpenTime time,
	CloseTime time
);

create table SupportAgent(
	ID int primary key,
	FirstName text,
	LastName text,
	phonenumber text,
	Address text
);

create table OperationAgent(
	ID int primary key,
	FirstName text,
	LastName text
);

create table DeliveryAgent(
	ID int primary key,
	FirstName text,
	LastName text,
	phonenumber text,
	isAvailable int,
	Credit int
);

create table DeliveryAgentStatusLog(
	DAID int,
	oldIsAvailable int,
	newISAvailable int,
	DateAndTime timestamp,
	foreign key (DAID) references DeliveryAgent(ID),
	primary key(DAID, DateAndTime)
);

create table StoreSupportAgent(
	StoreID int,
	SupportAgentID int,
	primary key(StoreID, SupportAgentID),
	foreign key (StoreID) references Store(ID),
	foreign key (SupportAgentID) references SupportAgent(ID)
);

create table StoreOperationAgent(
	StoreID int,
	OperationAgentID int,
	primary key(StoreID, OperationAgentID),
	foreign key (StoreID) references Store(ID),
	foreign key (OperationAgentID) references OperationAgent(ID)
);

create table StoreDeliveryAgent(
	StoreID int,
	DeliveryAgentID int,
	primary key(StoreID, DeliveryAgentID),
	foreign key (StoreID) references Store(ID),
	foreign key (DeliveryAgentID) references DeliveryAgent(ID)
);

create table StoreOrder(
	ID int primary key,
	StoreID int,
	CustomerUsername VARCHAR(20), 
	status int,
	PaymentType int,
	DateAndTime datetime,
	ithAddress int,
	DeliveryAgentID int,
	NotRegisteredUsersAddress text,
	foreign key (CustomerUsername, ithAddress) 
		references CustomerAddress(Username, ithAddress),
	foreign key (DeliveryAgentID) references DeliveryAgent(ID),
	foreign key (StoreID) references Store(ID)
);

create table StoreOrderStatusLog(
	OID int,
	oldStatus int,
	newStatus int,
	DateAndTime timestamp,
	foreign key (OID) references StoreOrder(ID),
	primary key(OID, DateAndTime)
);


create table Product(
	ID int primary key,
	title text,
	StoreID int,
	Price int,
	Discount int,
	Amount int,
	foreign key (StoreID) references Store(ID)
);

create table OrderProduct(
	OrderID int,
	ProductID int,
	Amount int,
	foreign key (OrderID) references StoreOrder(ID),
	foreign key (ProductID) references Product(ID),
	primary key (OrderID, ProductID)
);

create table ReasonForRejection(
	OrderID int primary key,
	reason text,
	foreign key (OrderID) references StoreOrder(ID)
);