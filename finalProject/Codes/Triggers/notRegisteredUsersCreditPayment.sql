DELIMITER $$
create trigger notRegisteredUsersCreditPayment
before update on StoreOrder for each row
begin
if new.status = 1 and old.status != 1 and 
   new.CustomerUsername is null and new.PaymentType = 0
then
	signal sqlstate '45000'
	set message_text = 
	'Not-registered users cannot pay with their credit.';
end if;
end;
$$
DELIMITER ;
