DELIMITER $$
create trigger decreaseAmountOfProductsAfterSending
before update on StoreOrder for each row
begin
if new.status = 2 and old.status != 2
then
update Product P
set P.Amount = P.Amount - (
	select OP.Amount 
	from OrderProduct OP 
	where OP.OrderID = new.ID and OP.ProductID = P.ID)
where P.ID in (
	select OP.ProductID
	from OrderProduct OP
	where OP.OrderID = new.ID);
end if;
end;
$$
DELIMITER ;