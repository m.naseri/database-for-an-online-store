DELIMITER $$
create trigger userCanOnlyChangeHisAccount
before update on Customer for each row
begin
if old.Username != SUBSTRING_INDEX(user(), '@', 1) and
 SUBSTRING_INDEX(user(), '@', 1) != 'root'
then
	signal sqlstate '45000'
	set message_text = 'You can only change your own account.';
end if;
end;
$$
DELIMITER ;
