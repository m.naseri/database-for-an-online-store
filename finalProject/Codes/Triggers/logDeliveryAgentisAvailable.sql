DELIMITER $$
create trigger logDeliveryAgentisAvailable
after update on DeliveryAgent for each row
begin
if new.isAvailable != old.isAvailable
then
	insert into DeliveryAgentStatusLog values
	(new.ID, old.isAvailable, new.isAvailable, now());
end if;
end;
$$
DELIMITER ;
