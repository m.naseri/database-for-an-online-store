DELIMITER $$
create trigger logStoreOrderStatus
after update on StoreOrder for each row
begin
if new.status != old.status
then
	insert into StoreOrderStatusLog values
	(new.ID, old.status, new.status, now());
end if;
end;
$$
DELIMITER ;
