DELIMITER $$
create trigger setDeliveryAgentAvailableAfterCompleted
after update on StoreOrder for each row
begin
if new.status = 3 and old.status != 3
then
update DeliveryAgent DA
set DA.isAvailable = 1,
	DA.Credit = DA.Credit + 
	(5/100)*(select B.price from Q3B B where B.OID = new.ID)
where DA.ID = new.DeliveryAgentID;
end if;
end;
$$
DELIMITER ;