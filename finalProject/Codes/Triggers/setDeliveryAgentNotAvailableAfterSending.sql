DELIMITER $$
create trigger setDeliveryAgentNotAvailableAfterSending
after update on StoreOrder for each row
begin
if new.status = 2 and old.status != 2
then
update DeliveryAgent DA
set DA.isAvailable = 0
where DA.ID = new.DeliveryAgentID;
end if;
end;
$$
DELIMITER ;