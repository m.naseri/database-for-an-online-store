DELIMITER $$
create trigger updateCreditOfCustomerSubmitOrder
after update on StoreOrder for each row
begin
if new.status = 1 and old.status != 1 and new.PaymentType = 0
and new.CustomerUsername is not null
then
update Customer
set Credit = Credit - (
	select B.price
	from Q3B B
	where B.OID = new.ID)
where Customer.Username = new.CustomerUsername;
end if;
end;
$$
DELIMITER ;