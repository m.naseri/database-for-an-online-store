DELIMITER $$
create trigger rejectClosedStore
before update on StoreOrder for each row
begin
if new.status = 1 and old.status != 1 and (
	time(new.DateAndTime) < 
	(select OpenTime from Store S where S.ID = new.StoreID)
	or 
	time(new.DateAndTime) > 
	(select CloseTime from Store S where S.ID = new.StoreID)	
)
then
	set new.status = -1;
	insert into ReasonForRejection values
		(new.ID, 'Store is closed now');	
end if;
end;
$$
DELIMITER ;
