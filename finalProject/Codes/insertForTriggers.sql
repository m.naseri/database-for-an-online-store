--updateCreditOfCustomerSubmitOrder
update Store set CloseTime = '16:30:00' where ID = 1;
update StoreOrder set PaymentType = 0 where ID = 2;
update StoreOrder set status = 0 where ID = 2;

--rejectClosedStore
update StoreOrder set status = 0 where ID = 2;
update Store set CloseTime = '14:00:00' where ID = 1;
update StoreOrder set status = 1 where ID = 2;

--notRegisteredUsersCreditPayment
update StoreOrder set PaymentType = 0, status = 1 where ID = 1;

--decreaseAmountOfProductsAfterSending
update StoreOrder set status = 2 where ID = 2;

--setDeliveryAgentNotAvailableAfterSending
update StoreOrder set status = 2 where ID = 2;

--setDeliveryAgentAvailableAfterCompleted
update StoreOrder set status = 3 where ID = 2;

--logStoreOrderStatus
select * from StoreOrderStatusLog;

--logDeliveryAgnteisAvailable
select * from DeliveryAgentStatusLog;

--userCanOnlyChangeHisAccount
insert into Customer values('C2Username', 'c2@gmail.com', SHA1('C2Pass'), 
	'C2Firstname', 'C2LastName', 'M', 1000, '1000000000');


create user 'C1Username'@'localhost' IDENTIFIED BY 'C1Pass';
grant update, select on naseriFinalDB.Customer to 'C1Username'@'localhost';
FLUSH PRIVILEGES;
update Customer set Password = SHA1('NewC1Pass') where Username = 'C1Username';