# Design and Implementing a Database for an Online Store (Fall 2018)
- Written in SQL using MySQL, Database course project, Dr. Hamid Reza Shahriari
- I was responsible for designing the schema of its database as well as implementing the schema in MySQL. The
Online Store had the ability to manage the amounts of products, orders, delivery agents, etc.
- For the second phase, I wrote some queries and triggers in order to do some tasks automatically in the absence
of a backend platform.